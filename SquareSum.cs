using System.Linq;
using System;

namespace codewars_dotnet
{
    public static class SquareSum
    {
        public static int Square(int[] n)
        {
            int sum = 0;
            for (int i = 0; i < n.Count(); i++)
            {
                sum += n[i] * n[i];
            }
            return sum;
        }
        public static int SquareSumTop(int[] n) => n.Sum(i => i * i);
        public static int SquareSumCasting(int[] n)
        {
            return (int)n.Sum(x => Math.Pow(x, 2));
        }
    }
}