using System;
using System.Collections.Generic;

namespace codewars_dotnet
{
    public class BubleSortOnce
    {
        public static int[] BubbleSortOnceMine(int[] input)
        {
            var sortedList = new List<int>(input);
            for (int i = 0; i < sortedList.Count-1; i++)
            {
                if (sortedList[i] > sortedList[i + 1])
                {
                    var temp = sortedList[i];
                    sortedList[i] = sortedList[i + 1];
                    sortedList[i + 1] = temp;
                }
                
            }
            // sortedList.ForEach(Console.WriteLine);
            return sortedList.ToArray();
        }
    }
}