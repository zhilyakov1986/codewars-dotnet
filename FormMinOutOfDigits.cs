using System;
using System.Collections.Generic;
using System.Linq;

namespace codewars_dotnet
{
    public class FormMinOutOfDigits
    {
        public static long MinValue(int[] a)
        {
            // implementing set to get rid of duplicates
            var valueSet = new HashSet<int> (a);
            // casting set to list of int to sort in asc order
            var valueInt = new List<int> (valueSet);
            valueInt.Sort();
            // casting list int to list of strings to perform concatenation
            var valueStr = valueInt.Select(str=>str.ToString()).ToList();
            var strResult = string.Join("", valueStr);
            // converting resulting string to long 
            var longResult = Convert.ToInt64(strResult);
            return longResult;
        }
        public static long MinValueTop(int[] a)
        {
            var res = string.Concat(a.OrderBy(x => x).Distinct());
            return Convert.ToInt64(res);
        }
    }
}