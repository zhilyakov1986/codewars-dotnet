// Kata.OddCount(7) => 3, i.e[1, 3, 5]
// Kata.OddCount(15) => 7, i.e[1, 3, 5, 7, 9, 11, 13]

namespace codewars_dotnet
{
    public class CountOdd
    {
        public static ulong OddCount(ulong n)
        {
                return n/2;
        }
    }
}