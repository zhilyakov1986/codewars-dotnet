namespace codewars_dotnet
{
    public class Person
    {
        public string Name;
        public void Indtroduce(string to)
        {
            System.Console.WriteLine($"Hi {to}, I am {Name}");
        }

        public static Person Parse(string str)
        {
            var person = new Person();
            person.Name = str;
            return person;
        }
    }
}