using System;
using System.Collections.Generic;
using System.Linq;
// for regex solution
using System.Text.RegularExpressions;


namespace codewars_dotnet
{
    public class Exclamation
    {
        public static string Remove(string s, int n)
        {
            var strList = s.Select(str => str.ToString()).ToList();
            while (n > 0)
            {
                var index = strList.IndexOf("!");
                if (index != -1)
                {
                    strList.RemoveAt(index);
                }
                n--;
            }
            return String.Join("", strList);
        }
        public static string RemoveRegex(string s, int n) =>
        new Regex("!").Replace(s, "", n);
    }
}