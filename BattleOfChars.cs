using System.Linq;

namespace codewars_dotnet
{
    public class BattleOfChars
    {
        public static string Battle(string x, string y)
        {
            const int ASCIItoNumber = 64;
            int sumX = 0;
            int sumY = 0;
            foreach (var letterX in x)
            {
                sumX += (int)letterX-ASCIItoNumber;
            }
            foreach (var letterY in y)
            {
                sumY += (int)letterY-ASCIItoNumber;
            }
            if (sumX>sumY)
            {
                return x;
            }
            else if (sumX<sumY){
                return y;
            }
            else
            {
                return "Tie!";
            }
        }
        public static string BattleTop(string x, string y)
        {
            int firstGroupPower = x.Sum(e => e - 65);
            int secondGroupPower = y.Sum(e => e - 65);

            return firstGroupPower > secondGroupPower ? x
              : firstGroupPower < secondGroupPower ? y
              : "Tie!";
        }
    }
}