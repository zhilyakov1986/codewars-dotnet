using System;
using System.Collections.Generic;
using System.Linq;

public class StringOperations
{
    public static string Remove_char(string s)
    {
        return s.Substring(1, s.Count() - 2);
    }
    public static string Remove_charTrending(string s)
    {
        return s.Length >= 2 ? s.Substring(1, s.Length - 2) : "ok";
    }
    public static bool Feast(string beast, string dish)
    {
        return beast[0] == dish[0] && beast[beast.Length - 1] == dish[dish.Length - 1] ? true : false;
    }
    public static string TwoSort(string[] s)
    {
        //sorting an array of strings
        Array.Sort(s, StringComparer.Ordinal);
        // var sortedArray = s.OrderBy(i=>i) ;
        var sortedList = new List<string>(s);
        // sortedList.ForEach(System.Console.WriteLine);
        var firstElement = sortedList[0];
        var result = "";
        for (int i = 0; i < firstElement.Length; i++)
        {
            if (i!=firstElement.Length-1)
            {
                result += $"{firstElement[i]}***";
            }
            else {
                result +=firstElement[i];
            }
        }
        return result;
    }
    public static string TwoSortTop(string[] s)
    {
        return string.Join("***", s.OrderBy(a => a, StringComparer.Ordinal).First().ToArray());
    }
}