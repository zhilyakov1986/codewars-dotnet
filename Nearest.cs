using System;
using System.Collections.Generic;
using System.Linq;

namespace codewars_dotnet
{
    public class Nearest
    {
        public static int[] RoundUp(int number, int[] list)
        {
            var nearestNums = new List<int>();
            var difference = int.MaxValue;
            if (list.Contains(number))
            {
                return list.Where(n => n == number).ToArray();
            }
            if (list.Length == 0)
            {
                return list;
            }
            for (int i = 0; i < list.Length; i++)
            {
                if (Math.Abs(list[i] - number) < difference)
                {
                    difference = Math.Abs(list[i] - number);
                }
            }
            for (int j = 0; j < list.Length; j++)
            {
                if (Math.Abs(list[j] - number) == difference)
                {
                    nearestNums.Add(list[j]);
                }

            }
            return nearestNums.ToArray();
        }

        public static int[] RoundUpTop(int number, int[] list) => list
      .GroupBy(x => Math.Abs(number - x))
      .OrderBy(x => x.Key)
      .FirstOrDefault()
      ?.ToArray() ?? new int[] { };
    }
}