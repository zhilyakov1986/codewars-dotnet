using System.Linq;

namespace codewars_dotnet
{
    public class LinqAverage
    {
        public static double LAverage(double[] array)
        {
            if (array.Length > 0)
            {
                return array.Average();
            }
            else
            {
                return 0;
            }
        }
        public static double FindAverageTop(double[] array)
        {
            return array.Length == 0 ? 0 : array.Average();
        }
    }
    
}