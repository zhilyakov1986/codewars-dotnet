namespace codewars_dotnet
{
    public class DutyFreeStore
    {
        public static int DutyFree(int normPrice, int Discount, int hol)
        {
            double discountedSavings = (double)normPrice * (double)Discount/100;
             var result = (double)hol / discountedSavings;
            return (int)result;
        }
        public static int DutyFreeTop(int normPrice, int Discount, int hol)
        {
            return (int)(hol / (normPrice * (Discount / 100f)));
        }
    }

}