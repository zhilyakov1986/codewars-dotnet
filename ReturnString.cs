namespace codewars_dotnet
{
    public static class ReturnString
    {
        // Write a public static method "greet" that returns "hello world!"
        public static string greet()
        {
            return "\"hello world!\"";
        }
    } 
}