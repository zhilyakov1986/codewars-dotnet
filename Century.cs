using System;

namespace codewars_dotnet
{

    public static class Century
    {
        public static int СenturyFromYear(int year)
        {
            if (year < 100)
            {
                return 1;
            }
            else if (year % 100 == 0)
            {
                return year / 100;
            }
            else
            {
                return year / 100 + 1;
            }

        }
    }
    public static class CenturyTop
    {
        public static int СenturyFromYear(int year) => (year - 1) / 100 + 1;
        public static int СenturyFromYearCeiling(int year)
        {
            return (int)Math.Ceiling(1.0 * year / 100);
        }
    }
    
}