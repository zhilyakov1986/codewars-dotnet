using System.Text.RegularExpressions;

namespace codewars_dotnet

{
    public class SubstituteChar
    {
        public static string Correct(string text)
        {
            text = text.Replace('0', 'O');
            text = text.Replace('1', 'I');
            text = text.Replace('5', 'S');
            return text;
        }
        public static string Replace(string s)
        {
            // replaces all vowels with exclamation sign
            return Regex.Replace(s, "[aeiouAEIOU]", "!");
        }
        public static string ReplaceTop(string s)
   => Regex.Replace(s, @"[aeiou]", "!", RegexOptions.IgnoreCase);
    }
}
