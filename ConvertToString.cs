using System;
namespace codewars_dotnet
{

    public class ConverToString
    {
        public static string NumberToString(int num)
        {
            return num.ToString();
        }
        public static string NumberToStringPopular(int num)
        {
            return $"{num}";
        }
        public static int SubtractSum(int number)
        {

            string strNum = number.ToString();
            var finalNum = 0;
            var sumOfString = 0;
            while (true)
            {
                for (int i = 0; i < strNum.Length; i++)
                {
                    sumOfString += int.Parse(strNum[i].ToString());
                }
                finalNum = number - sumOfString;
                if (sumOfString <= 100)
                {
                    break;
                }
                else
                {
                    strNum = sumOfString.ToString();
                }
            }
            return finalNum;
        }
    }
}