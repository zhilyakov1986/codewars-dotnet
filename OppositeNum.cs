using System;

namespace codewars_dotnet
{
    public class OppositeNum
    {
        public static int Opposite(int number)
        {
            return number > 0 ? -number : number * (-1);
        }
    }
}