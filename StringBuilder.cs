using System;
using System.Linq;
using System.Text;

namespace codewars_dotnet
{
    public class StringBuilder
    {
        public static string ToAlternatingCase(string s)
        {
            string changedString = "";
            foreach (var letter in s)
            {
                if (char.IsUpper(letter))
                {
                    changedString += letter.ToString().ToLower();
                }
                else
                {
                    changedString += letter.ToString().ToUpper();

                }
            }
            return changedString;
        }
        public static string ToAlternatingCaseTop(string Input)
        {
            return String.Join("", Input.ToCharArray().Select(character => Char.IsLower(character) ? Char.ToUpper(character) : Char.ToLower(character)));
        }
    }
}